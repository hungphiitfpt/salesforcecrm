public with sharing class AccountController {
    @AuraEnabled(cacheable=true)
    public static Object[] getProducts(String searchKey){
        String whereClause = '';
        String key = '';
            try {
                if(searchKey != null) {
                    if(!String.isEmpty(searchKey)){
                        key = '%' + searchKey + '%';
                        whereClause = 'WHERE NAME LIKE :key';
                    }
                }
            Object[] account = Database.query('Select Id, Name FROM Account ' + whereClause + 'WITH SECURITY_ENFORCED ' + 'ORDER BY Name');
            return account;
            } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}