({
    toggleCollapse : function(component, event, helper) {
        component.set('v.isCollapsed', !component.get('v.isCollapsed'));
    }
})