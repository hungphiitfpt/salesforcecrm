import { LightningElement,wire, track, api } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getProducts from '@salesforce/apex/AccountController.getProducts';
export default class AccountLightningWebComponent extends LightningElement {
    @track isModalOpen = false;

    @api
    searchable;
    @api
    recordId;
    @api
    objectApiName;

    @track searchKey = '';

    

    handleSearchKeyChange(event) {
        this.searchKey = event.target.value;
        console.log(event.target.value);
        console.log(accounts);
    }

    accounts = [];
    searcKey = '';
    
    @wire(getProducts, { searcKey: '$searchKey' })
    wiredProducts({ error, data }) {
        if (data) {
            this.accounts = data;
        } else if (error) {
            console.error(error);
        }
    }

    openModal() {
        this.isModalOpen = true;
    }

    closeModal() {
        this.isModalOpen = false;
    }
    handleSubmit(event) {
        event.preventDefault(); // ngăn chặn gửi form mặc định
        this.template.querySelector('lightning-record-edit-form').submit(event.detail.fields);
    }
    
    handleSuccess(event) {
        this.dispatchEvent(
            new ShowToastEvent({
                title: 'Success',
                message: event.detail.apiName + ' created.',
                variant: 'success',
            })
        );
    }

    
}