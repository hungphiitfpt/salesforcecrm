import { LightningElement , api , wire} from "lwc";
import getProducts from "@salesforce/apex/ProductController.getProducts"
export default class ProductList extends LightningElement {
	@api
	searchable;
	@api
	recordId;
	@api
	objectApiName;
	
	products = [];
	searchKey = '';
	
	@wire(getProducts, {
		searchKey : '$searchKey',
	})
	products;
	selectedProductId;
	handleProductSelected(event) {
		this.selectedProductId = event.detail;
	}
	handleSearchKeyChange(event) {
		this.searchKey = event.target.value;
	}
}