import { LightningElement, api } from "lwc";
export default class ProductTitle extends LightningElement {
	@api recordId;
	@api objectApiName;
}