import { api, LightningElement, wire } from 'lwc';
const productData = [
    {
        "Id": "product1",
        "Family": "Boot",
        "ProductCode":"PPD002",
        "MSRP_c" : 29002,
        "Description": "ABC"
    },
    {
        "Id": "product2",
        "Family": "Boot2",
        "ProductCode":"PPD004",
        "MSRP_c" : 233,
        "Description": "ABC2"
    }
]
export default class ProductLWC extends LightningElement {

  @api
  searchable;
  @api
  recordId;
  @api
  objectaApiName;
  products = productData;
}